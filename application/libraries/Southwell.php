<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CodeIgniter Southewell SMS endpoint Class
 *
 * This library will help import retrieve southwell SMS based on your user credentials  
 * 
 * This library treats the first row of a CSV file
 * as a column header row.
 * 
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Dindi Harris
 */
class Southwell {

    public function pull_messages($username, $password, $date_from, $date_to) {


        $URL = 'http://sms.southwell.io/api/v1/smsprintersent?limit=100&page=1&start_date=' . $date_from . '&end_date=' . $date_to . '';

        $httpRequest = curl_init($URL);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '));
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($httpRequest, CURLOPT_USERPWD, "$username:$password");
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);


        return $results;
    }

    public function send_message($username, $password, $recepient, $msg) {
        error_reporting(E_ERROR | E_PARSE);
//        $username = "smacharia@mhealthkenya.org";
//        $password = "works";
//        $num = "254714339521";
//
//        $msg = "mHealth Testing";
//        $senderid = "22108";
        $message = array("sender" => $senderid, "recipient" => $recepient, "message" => $msg);
        $URL = "http://104.155.214.144/fastSMSstage/public/api/v1/messages";
        $sms = json_encode($message);
        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $sms);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($sms)));
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($httpRequest, CURLOPT_USERPWD, "$username:$password");
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);
        $response = json_decode($results);
        return $response;
    }

}
